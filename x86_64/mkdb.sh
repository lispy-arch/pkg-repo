#!/usr/bin/env bash
rm -f lambda-linux-repo.{db,files}
repo-add -s -n -R lambda-linux-repo.db.tar.gz *.pkg.tar.zst 
rm -f lambda-linux-repo.db lambda-linux-repo.files
mv lambda-linux-repo.db.tar.gz lambda-linux-repo.db
mv lambda-linux-repo.files.tar.gz lambda-linux-repo.files
